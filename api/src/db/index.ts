import { Trip, Comment, Step } from 'common';
import path from 'path';
import low, { AdapterAsync, LowdbAsync } from 'lowdb';
import FileAsync from 'lowdb/adapters/FileAsync';

import { LOCAL_DB, PROD_DB } from '../constants';

interface Schema {
  trips: Trip[];
  steps: Step[];
  comments: Comment[];
}

const initial: Schema = {
  trips: [],
  steps: [],
  comments: [],
};

class Database {
  adapter: AdapterAsync;

  lowdb: LowdbAsync<Schema>;

  constructor() {
    const p = path.join(
      process.cwd(),
      'src',
      'db',
      process.env.NODE_ENV === 'production' ? PROD_DB : LOCAL_DB,
    );

    this.adapter = new FileAsync<Schema>(p);
  }

  init = async (): Promise<void> => {
    try {
      this.lowdb = await low(this.adapter);
      this.lowdb.defaults(initial).write();
    } catch (error) {
      console.log('error :>> ', error);
    }
  };
}

export default new Database();
