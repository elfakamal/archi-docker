import bodyParser from 'body-parser';
import express from 'express';
import morgan from 'morgan';

import api from './api';
import DB from './db';

const port = parseInt(process.env.PORT || '5000', 10);

const server = express();

server.use(morgan('tiny'));
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(api);

const bootstrap = async (): Promise<void> => {
  await DB.init();

  server.listen(port, (err) => {
    if (err) throw err;
    // eslint-disable-next-line no-console
    console.log(`> Ready on http://localhost:${port}`);
  });
};

bootstrap();
