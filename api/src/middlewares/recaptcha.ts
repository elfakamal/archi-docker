/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
import express from 'express';
import Axios from 'axios';

import { RECAPTCHA_SECRET_KEY } from '../../src/constants';

const app = express.Router();

app.use(async (req, res, next) => {
  const { token } = req.body;
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${RECAPTCHA_SECRET_KEY}&response=${token}`;

  try {
    const response = await Axios({
      url,
      method: 'post',
    });

    const googleResponse = response.data;
    // res.json({ googleResponse });

    next();
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
});
