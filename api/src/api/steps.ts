import express from 'express';
import { v4 } from 'uuid';

import DB from '../db';

const steps = express.Router();

const model = 'steps';

steps.get(`/${model}`, async (req, res): Promise<void> => {
  const { tripUUID } = req.query;

  if (!tripUUID) {
    res.status(400).json({
      status: 400,
      message: 'you must provide a valid trip uuid',
    });

    return;
  }

  const db = await DB.lowdb.read();
  const list = db.get(model)
    .filter((e) => e.tripUUID === tripUUID)
    .value();

  res.status(200).json(list);
});

steps.get(`/${model}/:uuid`, async (req, res): Promise<void> => {
  const { uuid } = req.params;
  const db = await DB.lowdb.read();
  const comment = db.get(model).find({ uuid }).value();
  res.status(200).json(comment);
});

steps.post(`/${model}`, async (req, res): Promise<void> => {
  const db = await DB.lowdb.read();
  const item = { uuid: v4(), ...req.body };
  db.get(model, []).push(item).write();
  res.status(201).json(item);
});

export default steps;
