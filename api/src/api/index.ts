import express from 'express';

import trips from './trips';
import steps from './steps';
import comments from './comments';

const api = express.Router();

api.use('/', [
  trips,
  steps,
  comments,
]);

export default api;
