import express from 'express';
import { v4 } from 'uuid';

import DB from '../db';

const trips = express.Router();

trips.get('/trips', async (req, res): Promise<void> => {
  const db = await DB.lowdb.read();
  const list = db.get('trips');
  res.status(200).json(list);
});

trips.get('/trips/:slug', async (req, res): Promise<void> => {
  const { slug } = req.params;

  const db = await DB.lowdb.read();

  const trip = db.get('trips').find({ title: slug }).value();
  res.status(200).json(trip);
});

trips.post('/trips', async (req, res): Promise<void> => {
  const db = await DB.lowdb.read();

  const item = {
    uuid: v4(),
    ...req.body,
  };

  db.get('trips', []).push(item).write();

  res.status(201).json(item);
});

export default trips;
