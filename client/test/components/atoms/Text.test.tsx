import { ReactWrapper, mount } from 'enzyme';
import React, { Component } from 'react';

import Text from '../../../src/components/atoms/Text';

interface Mount<C extends Component, P = C['props'], S = C['state']> {
  wrapper: ReactWrapper<P, S, C>;
  style: CSSStyleDeclaration;
}

const mountComponent = <C extends Component, P = C['props'], S = C['state']>(component): Mount<C, P, S> => {
  const wrapper = mount<C, P, S>(component);
  const style = getComputedStyle(wrapper.first().getDOMNode());

  return {
    wrapper,
    style,
  };
};

describe('ATOMS :: TEXT', () => {
  it('renders a message', () => {
    const { wrapper, style } = mountComponent(<Text>some text</Text>);
    expect(wrapper.text()).toMatch('some text');

    // rgb(104, 104, 104) is #686868 when computed
    expect(style.getPropertyValue('color')).toBe('rgb(104, 104, 104)');
    expect(style.getPropertyValue('font-size')).toBe('16px');
    expect(style.getPropertyValue('line-height')).toBe('inherit');
    expect(style.getPropertyValue('text-align')).toBe('inherit');
    expect(style.getPropertyValue('font-style')).toBe('inherit');
  });

  it('renders a bold message', () => {
    const { style } = mountComponent(<Text bold>some text</Text>);
    expect(style.getPropertyValue('font-weight')).toBe('bold');
  });

  it('renders an uppercase message', () => {
    const { style } = mountComponent(<Text uppercase>some text</Text>);
    expect(style.getPropertyValue('text-transform')).toBe('uppercase');
  });

  it('renders a france inter red colored message', () => {
    const { style } = mountComponent(<Text color="primary">some text</Text>);
    expect(style.getPropertyValue('color')).toBe('rgb(13, 125, 255)');
  });

  it('renders a big font sized message', () => {
    const { style } = mountComponent(<Text size="big">some text</Text>);
    expect(style.getPropertyValue('font-size')).toBe('24px');
  });

  it('renders a centered message', () => {
    const { style } = mountComponent(<Text align="center">some text</Text>);
    expect(style.getPropertyValue('text-align')).toBe('center');
  });

  it('renders a italic font style message', () => {
    const { style } = mountComponent(<Text fontStyle="italic">some text</Text>);
    expect(style.getPropertyValue('font-style')).toBe('italic');
  });

  it('renders a left padded message', () => {
    const { style } = mountComponent(<Text padded>some text</Text>);
    expect(style.getPropertyValue('padding-left')).toBe('10px');
  });

  it('renders a message with bottom margin', () => {
    const { style } = mountComponent(<Text bottomMargin="15px">some text</Text>);
    expect(style.getPropertyValue('margin-bottom')).toBe('15px');
  });

  it('renders a message with an ellipsis', () => {
    const { style } = mountComponent(<Text ellipsis>some text</Text>);

    expect(style.getPropertyValue('white-space')).toBe('nowrap');
    expect(style.getPropertyValue('overflow')).toBe('hidden');
    expect(style.getPropertyValue('text-overflow')).toBe('ellipsis');
  });
});
