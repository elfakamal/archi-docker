module.exports = {
  serverRuntimeConfig: {
    LOCAL_API: 'http://julien-api:5000',
  },
  publicRuntimeConfig: {
    LOCAL_API: 'http://localhost/api',
  },
  poweredByHeader: false,
};
