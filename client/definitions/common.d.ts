declare module 'common' {
  export interface Dict<T> {
    [key: string | number]: T;
  }

  interface Theme {
    screen: Dict<string>;
    color: Dict<string>;
    fontSize: Dict<string>;
    icon: Dict<string>;
    button: Dict<string>;
    layout: Dict<string>;
  }

  export interface HasTheme {
    theme?: Theme;
  }

  export interface HasUUID {
    uuid: string;
  }

  export interface HasTripUUID {
    tripUUID: string;
  }

  export interface HasTimestamps {
    createdAt?: number;
    updatedAt?: number;
  }

  export interface HasTitle {
    title: string;
  }

  export interface Trip extends HasUUID, HasTimestamps, HasTitle { }

  export interface Step extends HasUUID, HasTimestamps, HasTripUUID, HasTitle { }

  export interface Comment extends HasUUID, HasTimestamps, HasTripUUID {
    content: string;
  }
}
