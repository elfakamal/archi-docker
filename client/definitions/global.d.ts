interface GoogleRecaptchaExecuteOptions {
  action: string;
}

interface GoogleRecaptcha {
  ready: (fn: () => void) => void;
  execute: (key: string, options: GoogleRecaptchaExecuteOptions) => Promise<string>;
}

interface Window {
  grecaptcha: GoogleRecaptcha;
}
