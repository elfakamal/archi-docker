// eslint-disable-next-line @typescript-eslint/no-unused-vars
const NOOP = (...args: unknown[]): void => undefined;

export default NOOP;
