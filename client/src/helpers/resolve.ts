import { Dict } from 'common';

/**
 * Will get nested attributes from an object or an array
 * If not found, it will return the last argument if provided
 *
 * @param {Object} x
 * @param {string} path - A json path formated string
 * @param {unknown} notFound
 * @return {unknown}
 *
 * @author Kevin Combriat
 */
export default (obj?: Dict<unknown>, path?: string, notFound?: unknown): unknown => {
  if (!path || !obj) {
    return;
  }

  const pathParts = path.slice().split('.');

  // eslint-disable-next-line consistent-return
  return (function rec(x?: Dict<unknown>, part?: string, ...parts: unknown[]): unknown {
    if (!part && !parts.length) {
      return x;
    }

    if (x != null && !!part && part in x) {
      return rec(x[part] as object, ...parts as string[]);
    }

    return notFound;
  }(obj, ...pathParts));
};
