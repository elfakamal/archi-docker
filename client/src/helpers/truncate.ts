const truncate = (str: string, limit: number): string => (
  str.length > limit ? `${str.substr(0, limit)}...` : str
);

export default truncate;
