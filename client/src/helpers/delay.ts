const delay = (ms): Promise<void> => new Promise((r) => setTimeout(r, ms));

export default delay;
