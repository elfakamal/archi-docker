/* eslint-disable import/prefer-default-export */
import Axios from 'axios';
import { Comment } from 'common';
import getConfig from 'next/config';
import urlJoin from 'url-join';

const { publicRuntimeConfig } = getConfig();

export const getComments = async (tripUUID: string): Promise<Comment[]> => {
  try {
    const response = await Axios(urlJoin(publicRuntimeConfig.LOCAL_API, `/comments?tripUUID=${tripUUID}`));

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Comments :: getComments', 'error', error);

    return [];
  }
};

export const createComment = async ({ content, tripUUID }): Promise<Comment | null> => {
  try {
    const time = +((new Date()).getTime() / 1e3).toFixed();

    const response = await Axios({
      data: {
        createdAt: time,
        updatedAt: time,
        content,
        tripUUID,
      } as Comment,
      method: 'post',
      url: urlJoin(publicRuntimeConfig.LOCAL_API, 'comments'),
    });

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Comments :: createComment', 'error', error);

    return null;
  }
};
