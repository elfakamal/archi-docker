/* eslint-disable import/prefer-default-export */
import Axios from 'axios';
import { Step } from 'common';
import getConfig from 'next/config';
import urlJoin from 'url-join';

const { publicRuntimeConfig } = getConfig();

export const getSteps = async (tripUUID: string): Promise<Step[]> => {
  try {
    const url = urlJoin(publicRuntimeConfig.LOCAL_API, `/steps?tripUUID=${tripUUID}`);
    const response = await Axios(url);

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Steps :: getSteps', 'error', error);

    return [];
  }
};

type CreateStepParams = Pick<Step, 'title' | 'tripUUID'>;

type CreateStep = (p: CreateStepParams) => Promise<Step | null>;

export const createStep: CreateStep = async ({ title, tripUUID }) => {
  try {
    const time = +((new Date()).getTime() / 1e3).toFixed();

    const response = await Axios({
      data: {
        createdAt: time,
        updatedAt: time,
        title,
        tripUUID,
      } as Step,
      method: 'post',
      url: urlJoin(publicRuntimeConfig.LOCAL_API, 'steps'),
    });

    return response.data as Step;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Steps :: createStep', 'error', error);

    return null;
  }
};
