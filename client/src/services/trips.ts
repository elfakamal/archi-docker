/* eslint-disable import/prefer-default-export */
import Axios from 'axios';
import { Trip } from 'common';
import getConfig from 'next/config';
import urlJoin from 'url-join';

const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();

export const getTrips = async (): Promise<Trip[]> => {
  try {
    const response = await Axios(urlJoin(serverRuntimeConfig.LOCAL_API, 'trips'));

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Trips :: getTrips', 'error', error);

    return [];
  }
};

export const getTrip = async (slug: string): Promise<Trip | null> => {
  try {
    const response = await Axios(urlJoin(serverRuntimeConfig.LOCAL_API, 'trips', slug));

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Trips :: getTrip', 'error', error);

    return null;
  }
};

export const createTrip = async (title: string): Promise<Trip | null> => {
  try {
    const time = +((new Date()).getTime() / 1e3).toFixed();

    const response = await Axios({
      data: {
        createdAt: time,
        updatedAt: time,
        title,
        // token,
      },
      method: 'post',
      url: urlJoin(publicRuntimeConfig.LOCAL_API, 'trips'),
    });

    return response.data;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Services :: Trips :: createTrip', 'error', error);

    return null;
  }
};
