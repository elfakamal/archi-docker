import { RECAPTCHA_KEY } from '../constants';
import NOOP from '../helpers/noop';

const getRecaptchaToken = (): Promise<string> => new Promise((resolve, reject) => {
  (window.grecaptcha ? NOOP : reject)();
  ((window.grecaptcha || {}).ready || NOOP)(() => {
    ((window.grecaptcha || {}).execute || NOOP)(RECAPTCHA_KEY, { action: 'submit' }).then((token) => {
      resolve(token);
    });
  });
});

export default getRecaptchaToken;
