import { Trip } from 'common';
import React, { useCallback } from 'react';
import {
  Formik,
  FormikErrors,
  FormikHelpers,
  Form,
  Field,
} from 'formik';

import styled from 'styled-components';
import { createTrip } from '../../services/trips';
import NOOP from '../../helpers/noop';
import { getProp } from '../../theme';

interface CreateTripFormData {
  title: string;
}

interface CreateTripFormData {
  title: string;
}

interface Props {
  onComplete?: (trip: Trip | null) => void;
}

type OnSubmit = (
  values: CreateTripFormData,
  helpers: FormikHelpers<CreateTripFormData>,
) => Promise<void>;

const initialValues: CreateTripFormData = { title: '' };

const validate = (values: CreateTripFormData): FormikErrors<CreateTripFormData> => ({
  ...((!values.title || !values.title.trim()) ? { title: 'Required' } : {}),
} as FormikErrors<CreateTripFormData>);

const MyForm = styled(Form)`
  padding: 20px;
  background-color: #f5f5f5;
  border-radius: 10px;
  display: flex;
  width: 50%;
`;

const MyField = styled(Field)`
  border: none;
  outline: none;
  height: 50px;
  font-size: 24px;
  padding: 5px 10px;
  border-radius: 3px;
  flex-grow: 1;
`;

const MyButton = styled.button`
  box-sizing: border-box;
  width: 100px;
  margin-left: 20px;
  border: none;
  border-radius: 3px;
  font-size: 1rem;
  background-color: ${getProp('color.primary')};
  color: white;
`;


const TripForm = ({ onComplete }: Props): JSX.Element => {
  const onSubmit = useCallback<OnSubmit>(async (values, { setSubmitting }) => {
    setSubmitting(true);
    const createdTrip = await createTrip(values.title);
    setSubmitting(false);
    (onComplete || NOOP)(createdTrip);
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={onSubmit}
    >
      {({ isSubmitting }): JSX.Element => (
        <MyForm>
          <MyField type="title" name="title" />

          <MyButton type="submit" disabled={isSubmitting}>
            Search
          </MyButton>
        </MyForm>
      )}
    </Formik>
  );
};

export default TripForm;
