/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useCallback, RefObject } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

export interface PrefetchLinkProps {
  url: string;
  href: string;
  className?: string;
  children: React.ReactNode;
}

const PrefetchLink = React.forwardRef(({
  children,
  url,
  href,
  className,
}: PrefetchLinkProps, ref: RefObject<HTMLAnchorElement>): JSX.Element => {
  const router = useRouter();

  const prefetch = useCallback(() => {
    router.prefetch(url);
  }, [url]);

  return (
    <Link href={href} as={url} prefetch={false}>
      <a
        className={className}
        onMouseEnter={prefetch}
        ref={ref}
      >
        {children}
      </a>
    </Link>
  );
});

export default PrefetchLink;
