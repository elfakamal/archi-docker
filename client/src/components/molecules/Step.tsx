import React from 'react';
import { Step as IStep } from 'common';
import Text from '../atoms/Text';

interface Props {
  step: IStep;
}

const Step = ({ step }: Props): JSX.Element => (
  <Text>
    {step.title}
  </Text>
);

export default Step;
