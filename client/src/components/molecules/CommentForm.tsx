import { Comment } from 'common';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FormikErrors,
  FormikHelpers,
} from 'formik';
import React, { useCallback } from 'react';

import { createComment } from '../../services/comments';

interface Props {
  tripUUID: string;
  onSubmit: (c: Comment | null) => void;
}

interface CreateCommentFormData {
  content: string;
}

type OnSubmit = (
  values: CreateCommentFormData,
  helpers: FormikHelpers<CreateCommentFormData>,
) => Promise<void>;

const initialValues: CreateCommentFormData = { content: '' };

const validate = (values: CreateCommentFormData): FormikErrors<CreateCommentFormData> => ({
  ...((!values.content || !values.content.trim()) ? { content: 'Required' } : {}),
} as FormikErrors<CreateCommentFormData>);

/**
 * Comment form component
 *
 * @param props
 */
const CommentForm = ({ tripUUID, onSubmit: onComplete }: Props): JSX.Element => {
  const onSubmit = useCallback<OnSubmit>(async (values, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    const createdComment = await createComment({ content: values.content, tripUUID });
    setSubmitting(false);
    onComplete(createdComment);
    resetForm();
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={onSubmit}
    >
      {({ isSubmitting }): JSX.Element => (
        <Form>
          <Field type="content" name="content" />
          <ErrorMessage name="content" component="div" />

          <button type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default CommentForm;
