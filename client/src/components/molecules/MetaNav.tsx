import React from 'react';
import styled from 'styled-components';

import Flex from '../atoms/Flex';
import { getProp } from '../../theme';
import PrefetchLink from './PrefetchLink';
import SuperEllipse from '../atoms/SuperEllipse';

const Container = styled(Flex)`
  padding: 50px ${getProp('layout.margin')} 0;
`;

const Link = styled(PrefetchLink)`
  display: inherit;
  margin-right: 10px;
`;

const Logo = styled(SuperEllipse).attrs({
  width: 50,
  height: 50,
})`
  fill: ${getProp('color.primary')};
  filter: drop-shadow(1px 3px 10px hsla(240, 30.1%, 28%, 0.25));
`;

const MetaNav = (): JSX.Element => (
  <Container alignItems="center">
    <Link href="/" url="/">
      <Logo />
    </Link>
    <Link href="/trips" url="/trips">Trips</Link>
    <Link href="/trips/create" url="/trips/create">Create</Link>
  </Container>
);

export default MetaNav;
