import React from 'react';
import { Comment as IComment } from 'common';
import styled from 'styled-components';

import Flex from '../atoms/Flex';
import Text from '../atoms/Text';

interface Props {
  comment: IComment;
}

const Img = styled.img`
  max-width: 100%;
  height: auto;
  margin-right: 10px;

  border-radius: 4px;
  box-shadow: 0 13px 27px -5px hsla(240, 30.1%, 28%, 0.25),
              0 8px 16px -8px hsla(0, 0%, 0%, 0.3),
              0 -6px 16px -6px hsla(0, 0%, 0%, 0.03);
`;

const Comment = ({ comment }: Props): JSX.Element => (
  <Flex>
    <div>
      <Img src={`https://picsum.photos/40?random=${comment.uuid}`} alt="" />
    </div>
    <Text>
      {comment.content}
    </Text>
  </Flex>
);

export default Comment;
