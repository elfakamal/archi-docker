/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import styled from 'styled-components';

import Flex from '../atoms/Flex';
import { getProp } from '../../theme';

const Container = styled(Flex)`
  margin: 0 ${getProp('layout.margin')} 0;
`;

interface Props {
  children: React.ReactNode;
  className?: string;
}

const Page = ({ children, className }: Props): JSX.Element => (
  <Container column className={className}>
    {children}
  </Container>
);

export default Page;
