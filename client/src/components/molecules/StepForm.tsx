import { Step } from 'common';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FormikErrors,
  FormikHelpers,
} from 'formik';
import React, { useCallback } from 'react';

import { createStep } from '../../services/steps';

interface Props {
  tripUUID: string;
  onSubmit: (step: Step | null) => void;
}

interface CreateStepFormData {
  title: string;
}

type OnSubmit = (
  values: CreateStepFormData,
  helpers: FormikHelpers<CreateStepFormData>,
) => Promise<void>;

const initialValues: CreateStepFormData = { title: '' };

const validate = (values: CreateStepFormData): FormikErrors<CreateStepFormData> => ({
  ...((!values.title || !values.title.trim()) ? { title: 'Required' } : {}),
} as FormikErrors<CreateStepFormData>);

/**
 * Comment form component
 *
 * @param props
 */
const StepForm = ({ tripUUID, onSubmit: onComplete }: Props): JSX.Element => {
  const onSubmit = useCallback<OnSubmit>(async (values, { setSubmitting, resetForm }) => {
    const createdStep = await createStep({ title: values.title, tripUUID });
    setSubmitting(false);
    onComplete(createdStep);
    resetForm();
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={onSubmit}
    >
      {({ isSubmitting }): JSX.Element => (
        <Form>
          <Field type="title" name="title" />
          <ErrorMessage name="title" component="div" />

          <button type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default StepForm;
