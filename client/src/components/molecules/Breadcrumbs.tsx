/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

import Link from 'next/link';
import styled from 'styled-components';
import Text from '../atoms/Text';

interface Breadcrumb {
  title: string;
  url: string;
}

interface Props {
  path: Breadcrumb[];
}

const BreadcrumbsItem = styled.span`
  &:not(:last-child)::after {
    content: '>';
    display: inline-block;
    width: 20px;
    height: 20px;
    text-align: center;
  }
`;

const Breadcrumbs = ({ path }: Props): JSX.Element => (
  <Text>
    {path.map((p, i) => (
      <BreadcrumbsItem key={p.title}>
        <Link href={p.url}>
          <a>{p.title}</a>
        </Link>
      </BreadcrumbsItem>
    ))}
  </Text>
);

export default Breadcrumbs;
