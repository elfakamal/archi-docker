import React from 'react';
import styled from 'styled-components';

interface Props {
  src: string;
}

const MediaVisual = styled.figure`
  margin: 0;
  position: relative;
  background: #e9edf5;

  picture {
    /* display: flex; */
  }
`;

const Img = styled.img`
  /* width: 100%; */
  /* height: 121px; */
  margin: 0 auto;
  /* object-fit: cover; */
`;

const CardVisual = ({ src }: Props): JSX.Element => (
  <MediaVisual role="figure">
    <picture>
      <Img src={src} />
    </picture>
  </MediaVisual>
);

export default CardVisual;
