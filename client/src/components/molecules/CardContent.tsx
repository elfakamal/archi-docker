import { HasTheme } from 'common';
import React, { memo } from 'react';
import styled from 'styled-components';

import truncate from '../../helpers/truncate';
import { getProp, ThemeProp } from '../../theme';
import Flex from '../atoms/Flex';
import Text from '../atoms/Text';

interface Props extends HasTheme {
  title: string;
  date?: number;
  subtitle?: string;
}

const Container = styled(Flex).attrs({
  column: true,
})`
  margin: 16px;
  height: 100%;
  overflow: hidden;
`;

const SubtitleContainer = styled.div`
  font-size: 0;
`;

const Info = styled(Text)`
  font-size: calc(${(): ThemeProp => getProp('fontSize.small')} - 1px);
`;

const Title = styled(Text)`
  margin-top: 6px;
`;

const Subtitle = styled(Text).attrs({
  color: 'primary',
  ellipsis: true,
  uppercase: true,
})`
  max-width: 100%;
  font-size: 13px;
  line-height: 1.1;
  letter-spacing: 0.33px;

  &:focus,
  &:visited {
    color: ${getProp('color.primary')};
  }
`;

const options = {
  day: 'numeric',
  month: 'long',
  year: 'numeric',
};

const getDate = (timestamp: number): string => {
  const date = new Date(timestamp * 1e3);

  return date.toLocaleString('en-US', options);
};

const CardContent = memo(({
  date, title, subtitle, theme,
}: Props) => (
  <Container>
    {subtitle && (
      <SubtitleContainer>
        <Subtitle>{subtitle}</Subtitle>
      </SubtitleContainer>
    )}
    {date && <Info theme={theme}>{getDate(date)}</Info>}
    <Title color="dark_blue" fontFamily="bold">{truncate(title, 80)}</Title>
  </Container>
));

export default CardContent;
