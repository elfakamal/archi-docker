import { Trip } from 'common';
import Head from 'next/head';
import React from 'react';
import styled from 'styled-components';

import Flex from '../atoms/Flex';
import Title from '../atoms/Title';
import Page from '../molecules/Page';
import PrefetchLink from '../molecules/PrefetchLink';

const TripsContainer = styled(Flex).attrs({
  justifyContent: 'center',
  column: true,
})`
  margin: 0;
`;

interface Props {
  title: React.ReactNode;
  trips: Trip[];
}

const Trips = ({ trips }: Props): JSX.Element => (
  <Page>
    <Head>
      <title>Trips</title>
    </Head>

    <Title>Trips</Title>

    <TripsContainer>
      {trips.map((trip: Trip) => (
        <PrefetchLink
          key={trip.uuid}
          href={`/trips/${trip.title}`}
          url={`/trips/${trip.title}`}
        >
          {trip.title}
        </PrefetchLink>
      ))}
    </TripsContainer>
  </Page>
);

export default Trips;
