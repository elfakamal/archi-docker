import { Trip as ITrip } from 'common';
import React from 'react';
import styled from 'styled-components';
import Head from 'next/head';

import Title from '../atoms/Title';
import Page from '../molecules/Page';
import Feed from '../organisms/Feed';
import Journey from '../organisms/Journey';

interface Props {
  trip: ITrip;
}

export const Img = styled.img`
  max-width: 100%;
  height: auto;
  border-radius: 8px;
  box-shadow: 0 13px 27px -5px hsla(240, 30.1%, 28%, 0.25),
              0 8px 16px -8px hsla(0, 0%, 0%, 0.3),
              0 -6px 16px -6px hsla(0, 0%, 0%, 0.03);
`;

const Trip = ({ trip }: Props): JSX.Element => (
  <Page>
    <Head>
      <title>{(trip || {}).title || ''}</title>
    </Head>

    <Title>{(trip || {}).title || ''}</Title>
    <div>
      <Img src="https://picsum.photos/400/200" alt="" />
    </div>

    <Journey tripUUID={trip.uuid} />
    <Feed tripUUID={trip.uuid} />
  </Page>
);

export default Trip;
