import { Trip } from 'common';
import React, { useCallback } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

import Page from '../molecules/Page';
import Title from '../atoms/Title';
import TripForm from '../molecules/TripForm';

/**
 * Create Trip template
 */
const Create = (): JSX.Element => {
  const router = useRouter();

  const onComplete = useCallback((trip: Trip) => {
    router.push('/trips/[slug]', `/trips/${trip.title}`);
  }, []);

  return (
    <Page>
      <Head>
        <title>Create</title>
      </Head>

      <Title>Create</Title>
      <TripForm onComplete={onComplete} />
    </Page>
  );
};

export default Create;
