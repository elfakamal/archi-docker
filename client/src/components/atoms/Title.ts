import styled from 'styled-components';

import Text from './Text';
import { getProp } from '../../theme';

const Title = styled(Text).attrs({ as: 'h1' })`
  line-height: 1.15;
  font-size: 4rem;
  color: ${getProp('color.primary')};
`;

export default Title;
