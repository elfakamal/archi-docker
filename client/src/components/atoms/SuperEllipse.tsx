/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-props-no-spreading */
import React, { SVGProps } from 'react';

const SuperEllipse = ({
  className,
  color,
  width = 100,
  height = 100,
  children,
}: SVGProps<SVGSVGElement>): JSX.Element => (
  <svg
    viewBox={`0 0 ${width} ${height}`}
    width={width}
    height={height}
    className={className}
  >
    <path
      fill={color}
      stroke="none"
      d={`M 0,${+height / 2}
      Q 0,0 ${+width / 2},0
      ${+width},0 ${+width},${+height / 2}
      ${+width},${+height} ${+width / 2},${+height}
      0,${+height} 0,${+height / 2}`}
    />
    {children}
  </svg>
);

export default SuperEllipse;
