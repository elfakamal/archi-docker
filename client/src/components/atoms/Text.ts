import { HasTheme } from 'common';
import styled, {
  css,
  FlattenSimpleInterpolation,
  FlattenInterpolation,
  ThemedStyledProps,
} from 'styled-components';

import { getProp, ThemeProp } from '../../theme';

export interface TextProps extends HasTheme {
  color?: string;
  size?: string;
  lineHeight?: string;
  align?: string;
  fontStyle?: string;
  fontFamily?: string;
  uppercase?: boolean;
  bold?: boolean;
  onClick?: () => void;
  padded?: boolean;
  bottomMargin?: string;
  hover?: boolean;
  ellipsis?: boolean;
}

const Text = styled.div<TextProps>`
  color: ${(props): ThemeProp => getProp(`color.${props.color || 'dark_grey'}`)};
  font-size: ${(props): ThemeProp => getProp(`fontSize.${props.size || 'standard'}`)};
  line-height: ${(props): string => props.lineHeight || 'inherit'};
  text-align: ${(props): string => props.align || 'inherit'};
  font-style: ${(props): string => props.fontStyle || 'inherit'};

  ${(props): FlattenSimpleInterpolation | string => (props.uppercase ? css`text-transform: uppercase;` : '')}
  ${(props): FlattenSimpleInterpolation | string => (props.bold ? css`font-weight: bold;` : '')}
  ${(props): FlattenSimpleInterpolation | string => (props.onClick ? css`cursor: pointer;` : '')}
  ${(props): FlattenSimpleInterpolation | string => (props.padded ? css`padding-left: 10px;` : '')}
  ${(props): FlattenSimpleInterpolation | string => (props.bottomMargin ? css`margin-bottom: ${props.bottomMargin};` : '')}

  ${(props): FlattenInterpolation<ThemedStyledProps<HasTheme, unknown>> | string => (props.hover ? css`
    &:hover {
      color: ${getProp('color.text_hover')};
      cursor: pointer;
    }
  ` : '')}

  ${(props): FlattenSimpleInterpolation | string => (props.ellipsis ? css`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  ` : '')}
`;

export default Text;
