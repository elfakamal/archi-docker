/* eslint-disable react/jsx-props-no-spreading */
import React, { SVGProps } from 'react';

const PauseIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg viewBox="0 0 42 42" {...props}>
    <path fill="#005cd3" d="M21 .1C9.457.1.1 9.457.1 21S9.457 41.9 21 41.9 41.9 32.543 41.9 21 32.543.1 21 .1z" />
    <path d="M17 14h-1c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h1c1.1 0 2-.9 2-2V16c0-1.1-.9-2-2-2zm9 0h-1c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h1c1.1 0 2-.9 2-2V16c0-1.1-.9-2-2-2z" fill="#FFF" />
  </svg>
);

export default PauseIcon;
