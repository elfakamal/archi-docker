/* eslint-disable react/jsx-props-no-spreading */
import React, { SVGProps } from 'react';

const PlayIcon = (props: SVGProps<SVGSVGElement>): JSX.Element => (
  <svg viewBox="0 0 42 42" {...props}>
    <path
      fill="#005cd3"
      d="M21 .1C9.457.1.1 9.457.1 21S9.457 41.9 21 41.9 41.9 32.543 41.9 21 32.543.1 21 .1z"
    />
    <path
      fill="#FFF"
      d="M18.583 27.201c-1.421.839-2.583.176-2.583-1.474v-9.364c0-1.65 1.162-2.313 2.583-1.474l7.834 4.629c1.421.839 1.421 2.213 0 3.052l-7.834 4.631z"
    />
  </svg>
);

export default PlayIcon;
