import styled, { css, FlattenSimpleInterpolation } from 'styled-components';

import View, { ViewProps } from './View';

interface Props extends ViewProps {
  alignItems?: string;
  justifyContent?: string;
  column?: boolean;
  grow?: boolean;
  stretch?: boolean;
  onClick?: () => void;
}

const Flex = styled(View)`
  display: flex;
  align-items: ${(props: Props): string => props.alignItems || 'normal'};
  justify-content: ${(props: Props): string => props.justifyContent || 'flex-start'};

  ${(props: Props): FlattenSimpleInterpolation | string => (props.column ? css`flex-direction: column;` : '')}
  ${(props: Props): FlattenSimpleInterpolation | string => (props.grow ? css`flex-grow: 1;` : '')}
  ${(props: Props): FlattenSimpleInterpolation | string => (props.stretch ? css`align-self: stretch;` : '')}
  ${(props: Props): FlattenSimpleInterpolation | string => (props.onClick ? css`cursor: pointer;` : '')}
`;

Flex.displayName = 'Flex';

export default Flex;
