import React from 'react';
import styled from 'styled-components';

interface Props {
  children?: React.ReactNode;
  type?: 'button' | 'submit' | 'reset' | undefined;
  disabled?: boolean;
}

const StyledSuperEllipse = styled.button`
  width: 150px;
  height: 30px;
  border: 0;
  outline: none;
  background-color: transparent;
  position: relative;
  color: white;
  cursor: pointer;

  &::before {
    content: attr(data-cta);
    font-size: 1.618rem;
    line-height: 90px;
    position: absolute;

    border-radius: 2% / 50%;
    background-color: black;
    top: 2px;
    bottom: 2px;
    right: -2px;
    left: -2px;
  }

  &::after {
    content: '';
    position: absolute;
    border-radius: 50% / 10%;

    background-color: black;
    top: -1px;
    bottom: -1px;
    right: 0px;
    left: 0px;
  }

  > * {
    z-index: 1;
    position: relative;
  }
`;

const SuperEllipseWithCSS = ({ children, type, disabled }: Props): JSX.Element => (
  <StyledSuperEllipse type={type} disabled={disabled}>
    <div>{children}</div>
  </StyledSuperEllipse>
);

export default SuperEllipseWithCSS;
