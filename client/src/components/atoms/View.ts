import styled from 'styled-components';

export interface ViewProps {
  fullSize?: boolean;
  fullWidth?: boolean;
  fullHeight?: boolean;
  position?: string;
}

const View = styled.div`
  ${(props: ViewProps): string => ((props.fullWidth || props.fullSize) ? 'width: 100%;' : '')}
  ${(props: ViewProps): string => ((props.fullHeight || props.fullSize) ? 'height: 100%;' : '')}
  ${(props: ViewProps): string => (props.position ? `position: ${props.position};` : '')}
`;

export default View;
