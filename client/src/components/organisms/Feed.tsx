import { Comment as IComment } from 'common';
import React, { useCallback, useState, useEffect } from 'react';

import Comment from '../molecules/Comment';
import Flex from '../atoms/Flex';
import CommentForm from '../molecules/CommentForm';
import { getComments } from '../../services/comments';

interface Props {
  tripUUID: string;
}

const Feed = ({ tripUUID }: Props): JSX.Element => {
  const [comments, setComments] = useState<IComment[]>([]);

  const load = useCallback(async (id: string) => {
    setComments(await getComments(id));
  }, [tripUUID]);

  useEffect(() => {
    load(tripUUID);
  }, []);

  return (
    <Flex column>
      <h3>Comments</h3>

      {(comments || []).map((comment) => (
        <Comment
          key={comment.uuid}
          comment={comment}
        />
      ))}
      <CommentForm
        tripUUID={tripUUID}
        onSubmit={(): Promise<void> => load(tripUUID)}
      />
    </Flex>
  );
};

export default Feed;
