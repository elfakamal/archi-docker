/* eslint-disable @typescript-eslint/ban-ts-ignore */
import React, { useCallback, useEffect, useState } from 'react';
import { HasTripUUID, Step as IStep } from 'common';

import { getSteps } from '../../services/steps';
import Step from '../molecules/Step';
import Flex from '../atoms/Flex';
import StepForm from '../molecules/StepForm';

const Journey = ({ tripUUID }: HasTripUUID): JSX.Element => {
  const [steps, setSteps] = useState<IStep[]>([]);

  const load = useCallback(async (id: string) => (
    setSteps(await getSteps(id))
  ), [tripUUID]);

  useEffect(() => {
    load(tripUUID);
  }, []);

  return (
    <Flex column>
      <h3>Journey</h3>

      {(steps || []).map((step) => (
        <Step
          key={step.uuid}
          step={step}
        />
      ))}

      <StepForm
        tripUUID={tripUUID}
        onSubmit={(): Promise<void> => load(tripUUID)}
      />
    </Flex>
  );
};

export default Journey;
