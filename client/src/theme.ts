import { Dict, HasTheme, Theme } from 'common';

import resolve from './helpers/resolve';

export const THEME: Theme = {
  screen: {
    xs_max: '767px',
    sm_min: '768px',
    md_min: '1024px',
    lg_min: '1200px',
  },

  color: {
    // primary: '#0D7DFF',
    primary: '#14AAC0',
    grey: '#f0f0f0',
    dark_grey: '#686868',
    darker_grey: '#161616',
    white: '#FFFFFF',
    dark_blue: '#292C35',
    transparent_dark_blue: 'rgb(41, 44, 53, 0.95)',
    light_blue: '#e5e9f2',
    text_hover: '#292C35',
  },

  fontSize: {
    tiny: '10px',
    input: '12px',
    small: '14px',
    standard: '16px',
    medium: '18px',
    title: '20px',
    large: '22px',
    big: '24px',
    very_big: '30px',
    xxl: '40px',
  },

  icon: {
    height: '24px',
    width: '24px',
  },

  button: {
    min_width: '200px',
  },

  layout: {
    margin: '100px',
  },
};

const isObjectEmpty = <T extends object>(o: T): boolean => (
  Object.keys(o).length === 0 && o.constructor === Object
);

export type ThemeProp = (props: HasTheme) => string;

export const getProp = <T extends HasTheme>(path: string): ThemeProp => ({ theme }: Pick<T, 'theme'>): string => {
  const actualTheme = !theme || isObjectEmpty(theme as Dict<string | object>) ? THEME : theme;

  return resolve(actualTheme, path) as string;
};
