/* eslint-disable react/jsx-props-no-spreading */
import { AppProps } from 'next/app';
import React from 'react';
import { ThemeProvider, createGlobalStyle } from 'styled-components';

import { THEME } from '../theme';
import MetaNav from '../components/molecules/MetaNav';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;

    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
      Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
      sans-serif;
  }

  * {
    box-sizing: border-box;
  }
`;

const NomadApp = ({ Component, pageProps }: AppProps): JSX.Element => (
  <ThemeProvider theme={THEME}>
    <GlobalStyle />
    <MetaNav />
    <Component {...pageProps} />
  </ThemeProvider>
);

export default NomadApp;
