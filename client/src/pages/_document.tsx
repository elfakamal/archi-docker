import React from 'react';
import Document, {
  Head,
  Main,
  NextScript,
  DocumentContext,
  DocumentInitialProps,
} from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
    const sheets = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    ctx.renderPage = () => originalRenderPage({
      enhanceApp: (App) => (props): React.ReactElement<{ sheet: ServerStyleSheet }> => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        sheets.collectStyles(<App {...props} />)
      ),
    });

    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
      styles: [
        ...React.Children.toArray(initialProps.styles),
        sheets.getStyleElement(),
      ],
    };
  }

  render(): JSX.Element {
    return (
      <html lang="fr">
        <Head>
          <meta charSet="UTF-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover" />
          <meta name="theme-color" content="#005cd3" />
          <meta name="msapplication-navbutton-color" content="#005cd3" />
          <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

          <link rel="icon" href="/favicon.ico" />
          {this.props.styles}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
