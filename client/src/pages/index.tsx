import Head from 'next/head';
import React, { useCallback, useState, useContext } from 'react';
import styled from 'styled-components';

import RawTitle from '../components/atoms/Title';
import delay from '../helpers/delay';
import NOOP from '../helpers/noop';

interface Dockable {
  docked?: boolean;
}

const Title = styled(RawTitle)<Dockable>`
  position: absolute;
  top: 200px;
  left: 100px;

  transition: .5s opacity ease;
  opacity: ${(props): string => (props.docked ? '0' : '1')};
`;

const Index = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>Hello</title>
      </Head>

      <Title>
        Hello
      </Title>

    </>
  );
};

export default Index;
