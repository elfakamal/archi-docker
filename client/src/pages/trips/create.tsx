import React from 'react';

import Create from '../../components/templates/Create';

/**
 * Create Trip page
 */
const CreatePage = (): JSX.Element => (
  <Create />
);

export default CreatePage;
