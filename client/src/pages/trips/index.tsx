import { Trip } from 'common';
import { GetServerSideProps } from 'next';
import React from 'react';

import Trips from '../../components/templates/Trips';
import { getTrips } from '../../services/trips';

interface Props {
  trips: Trip[];
}

const TripsPage = ({ trips }: Props): JSX.Element => (
  <Trips
    title="Trips"
    trips={trips}
  />
);

export default TripsPage;

export const getServerSideProps: GetServerSideProps = async () => ({
  props: {
    trips: await getTrips(),
  },
});
