import React from 'react';
import { Trip as ITrip } from 'common';
import { ParsedUrlQuery } from 'querystring';
import { GetServerSideProps } from 'next';

import { getTrip } from '../../services/trips';
import Trip from '../../components/templates/Trip';

interface Props {
  trip: ITrip;
}

const TripPage = ({ trip }: Props): JSX.Element => (
  <Trip trip={trip} />
);

interface Params extends ParsedUrlQuery {
  slug: string;
}

export const getServerSideProps: GetServerSideProps<{}, Params> = async ({ params }) => ({
  props: {
    trip: params ? await getTrip(params.slug || '') : {},
  },
});

export default TripPage;
